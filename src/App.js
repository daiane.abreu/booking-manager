import {Route, Routes} from 'react-router-dom'
import HomeView from './views/Home'
import FlightsList from './views/FlightsList';
function App() {
  return (
    <Routes>
      <Route path="/" element={<HomeView/>}/>
      <Route path="/FlightsList" element={<FlightsList/>}/>
    </Routes>
  );
}

export default App;
