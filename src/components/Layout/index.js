import { Header } from './Header'
import Rodape from './Footer'

export function Layout ({ children }) {
  return (
    <>
      <Header />
      <main>
        {children}
      </main>
      <Rodape />
    </>
  )
}
